import React,{useState,useEffect} from 'react';
import {NativeSelect,FormControl} from '@material-ui/core';
import styles from './Country.module.css';
import {fetchCountries} from '../../api'; 

const Country = ({changeValue}) => {
    const [country ,setCountry] = useState([])
    //i think componentdidmount only can one time?
    //u can see there is 2 parameter in useEffect which is [setCountry] for purpose then there can watch the changes in selection picker when setCountry
    useEffect(() => {
        const fetchCountriesAPI = async() =>{
            setCountry(await fetchCountries())
        }
        console.log("Country:",country)
        fetchCountriesAPI();
    },[setCountry])
    return(
        <FormControl className={styles.formControl}>
            <NativeSelect onChange={(e)=>changeValue(e.target.value)}> 
                <option value="">Global</option>
                {country.map((country,i)=><option key={i} value={country} >{country}</option>)}
            </NativeSelect>
        </FormControl>
    )
}

export default Country;