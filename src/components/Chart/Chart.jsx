import React,{useState,useEffect} from 'react';
import styles from './Chart.module.css';
import {Line , Bar} from 'react-chartjs-2';
import {fetchDailyData} from '../../api';

const Chart = ({data:{confirmed,recovered,deaths},country}) => {
    const [dailyData,setDailyData] = useState([])
    
    //useEffect will "call every time" after it render
    useEffect(() => {
        const fetchAPI = async() =>{
            setDailyData(await fetchDailyData())
        }
        // console.log("hope",dailyData)
        fetchAPI();
    })
    //declare variable and it have design ()
    const LineChart =(
        dailyData.length?(
        <Line
            data={{
                //the code below is like the mapping function ,loop and display the data
                labels:dailyData.map(({date}) => date),
                datasets:[{
                    data:dailyData.map(({confirmed}) => confirmed),
                    label:'Infected',
                    borderColor:'blue',
                    fill:true
                },{
                    data:dailyData.map(({deaths}) => deaths),
                    label:'Deaths',
                    borderColor:'red',
                    backgroundColor:'rgba(255,0,0,0.5)',
                    fill:true
                }]
            }}
        />): null
    );
    
    const barChart =(
        confirmed?
        (<Bar 
            data={{
                labels:['Infected','Recovered','Deaths'],
                datasets:[{
                    label:'People',
                    backgroundColor:["blue","green","red"],
                    data:[confirmed.value,recovered.value,deaths.value]
                }]
            }}
            options={{
                legend:{display:false},
                title:{display:true,text:"Current State in "+country}
            }}
        /> ):null
    )

    return(
        <div className = {styles.container}>
            {country?barChart:LineChart}
        </div>
    )
}

export default Chart;